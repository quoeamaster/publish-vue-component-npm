// main entry point to various components

import * as components from './components'
// include css for building purpose (as this is the main entry file)
import './tailwind.css'

const defaultComponents = components?.default
// plugin-able
const pubComponents = {
  install(Vue) {
    // register the components into the Vue object / app; hence can use it like a normal "tag"
    Object.keys(defaultComponents).forEach(name => {
      Vue.component(name, defaultComponents[name])
    })
  }
}
export default pubComponents

// tree shaking; only need a component instead of a bunch of them (dedicated)
export { Card } from './components/card'