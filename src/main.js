import { createApp } from 'vue'
import App from './App.vue'
import './tailwind.css'

// testing purpose - if not the App.vue and this file is meaningless
createApp(App).mount('#app')
