import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import postcss from 'rollup-plugin-postcss'


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // add the following for rollup to build a library instead of an "app"
  build: {
    lib: {
      // the entry point for all exported components
      entry: resolve(__dirname, 'src/index.js'),
      formats: ['es', 'cjs'],
      fileName: (format) => `publish-vue-component-npm-card.${format}.js`,
    }, // end - lib
    rollupOptions: {
      // don't bundle "vue" into the final js
      external: ['vue'],
      output: {
        // support tree shaking (dedicated import)
        preserveModules: true,
        exports: 'named',
      },
      // post css plugin
      plugins: [
        postcss({
          // bug?? the file created is still style.css
          //extract: resolve('dist/index.css'),
          extract: true,
          extensions: ['css'],
          modules: true,
        }),
      ],

    }, // end - rollupOptions

    /** added for tailwindCss bundling */
    sourcemap: true,
    emptyOutDir: true,
  }, // end - build
})
